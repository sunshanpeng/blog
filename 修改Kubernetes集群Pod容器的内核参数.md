---
title: 修改Kubernetes集群Pod容器的内核参数
date: 2019-02-24 22:29:37
categories:  
 - 容器
tags: 
 - k8s

---

容器的本质是一个进程，共享Node的内核。原以为修改了Node的内核参数容器中也会改，但实际上并不是这样，容器的内核参数可以和Node不同。

## Docker Daemon

```bash
docker run -it --rm --sysctl net.core.somaxconn=65535 busybox cat /proc/sys/net/core/somaxconn
```

多个参数：

```bash
docker run -itd --restart=always --net=host \
--name=centos01 --hostname=centos01 \
--sysctl kernel.msgmnb=13107200 \
--sysctl kernel.msgmni=256 \
--sysctl kernel.msgmax=65536 \
--sysctl kernel.shmmax=69719476736 \
--sysctl kernel.sem='500 256000 250 1024' \
-v /mnt:/update \
centos /bin/bash
 
 
docker exec centos01 sysctl -a |grep -E \
'kernel.msgmnb|kernel.msgmni|kernel.msgmax|kernel.shmmax|kernel.sem'
```

## Kubernetes

### Kubernetes Sysctls

1. 增加Kubelet启动参数
```
kubelet --allowed-unsafe-sysctls=net.*
```
2. 重新加载配置
```
systemctl daemon-reload
```
3. 重启kubelet
```
systemctl restart kubelet
```
4. 配置Pod的securityContext参数（注意是pod不是container）

```
      securityContext:
        sysctls:
        - name: net.ipv4.tcp_keepalive_time
          value: "180"
```
Kubernetes允许配置的内核参数如下：
```
kernel.shm*,
kernel.msg*,
kernel.sem,
fs.mqueue.*,
net.*.
```
> 使用Kubernetes Sysctls推荐指定几台Node，利用污点或者节点亲和性把需要修改内核参数的pod调度到指定节点，而不是修改所有Node。


### Kubernetes Init Container
使用init container不需要修改kubelet参数
```yaml
      initContainers:
      - command:
        - sysctl
        - -w
        - net.ipv4.tcp_keepalive_time=180
        image: busybox:1.27
        name: init-sysctl
        securityContext:
          privileged: true
```


## 参考资料

- https://www.cnblogs.com/DaweiJ/articles/8528687.html
- https://yq.aliyun.com/articles/603745?utm_content=m_1000003707
- https://kubernetes.io/docs/tasks/administer-cluster/sysctl-cluster/
---
title: Kubectl常用命令
date: 2019-02-24 22:26:37
categories:  
 - 容器
tags: 
 - k8s

---

## 运行容器

### 前台运行的容器

```bash
kubectl run -it --rm --image=centos --restart=Never test bash
```

### 常驻后台的容器

```bash
kubectl run nginx --image=nginx --replicas=2
```

通常使用yaml文件创建容器，只在调试或者排错的时候使用`kubectl run`临时创建容器。

比如：

### 创建网络排查容器

```bash
kubectl run -it --rm --restart=Never --image=infoblox/dnstools:latest dnstools
```

如果创建了一个MySQL容器并且暴露了service，但是不能使用service访问mysql，这个时候可以使用`nslookup`或者`dig`这些网络命令排查。

```bash
dnstools# nslookup mysql
Server:         172.21.0.2
Address:        172.21.0.2#53

Name:   mysql.qa.svc.cluster.local
Address: 172.21.147.86
```

### 创建MySQL排查容器

```bash
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql --port 3306 -u root -p123456
```

如果创建的MySQL容器在集群外连接不上了，可以创建一个mysql容器在内部连接看看能不能连上。

- `-h`参数是MySQL的host；
- `--port`参数是MySQL的端口号；
- `-u`参数是MySQL的用户名；
- `-p`参数是用户名对应的密码，`-p`和密码之间没有空格。

## 应用资源

```bash
kubectl apply -f deployment.yaml
```

推荐使用这种方式创建或者更新资源。

## 获取容器

```bash
kubectl get pods --all-namespaces -o wide
```

获取所有namespace的容器。

- `--all-namespaces`表示所有namespace，默认获取`default namespace`的资源，可以通过`-n`指定namespace。
- `-o wide`表示输出格式，其他还要`json`，`yaml`。

### 获取所有不是Running状态的容器

```bash
kubectl get pods --all-namespaces -o wide | awk '{if ($4 != "Running") print $0}'
```

###  获取其他资源

除了容器，还有很多其他资源例如`node`、`service`、`deployment`、`statefulset`、`daemonset`、`job`、`pvc`、`pv`等等可以通过kubectl来获取，大多数是区分namespace的，也有不区分namespace的比如`node`、`pv`、`storageclass`等。

## 删除容器

```bash
kubectl delete pod nginx-deployment-599c95f496-hd2jc
```

### 强制删除：

```bash
kubectl delete pod nginx-deployment-599c95f496-hd2jc --force --grace-period=0
```

### 批量强制删除：

```bash
kubectl get pods | grep Terminating | awk '{print $1}' | xargs kubectl delete pod --force --grace-period=0
```

### 批量强制删除非运行容器

```bash
kubectl get pods --all-namespaces | awk '{if ($4 != "Running") system ("kubectl -n " $1 " delete pods " $2 " --grace-period=0 " " --force ")}'
```

## 扩缩容

```bash
kubectl scale deployment nginx --replicas 4
```

最小可以缩容到0个。

## 暂停滚动升级

```bash
kubectl rollout pause deployment nginx
```

滚动升级时，可以使用该命令暂停升级来实现金丝雀发布。

## 恢复滚动升级

```bash
kubectl rollout resume deployment nginx
```

暂停后恢复。

## 回滚

```bash
kubectl rollout undo deployment nginx
```

回滚到上一次发布。

## 给node加taint（污点）

```bash
kubectl taint nodes node1 key=value:NoSchedule
```

给节点添加污点后只有容忍了该污点的容器才能调度上来。

## 查看node信息

```bash
kubectl describe nodes node1
```

通过该命令可以查看node的资源、内核、容器、标签和污点等等。

## 给node加标签

```bash
kubectl label node node1 kubernetes.io/role=node --overwrite
```

给node加标签后可以用节点亲和性指定某些pod调度到固定node。

## 删除node上的标签

```bash
kubectl label node node1 kubernetes.io/role-
```

## 根据标签筛选

```bash
kubectl get nodes -l node-type=iot
```

## 禁止节点调度

### 只禁止不驱逐

```bash
kubectl cordon node1
```

这种方式只把node标记为`SchedulingDisabled`,已经在node上运行的pod不会受影响，之后不会再有新的pod调度上去。

### 禁止并驱逐

```bash
kubectl drain node1 --ignore-daemonsets --delete-local-data --force
```

这种方式除了把node标记为`SchedulingDisabled`，已经运行的pod也会被驱逐，保证节点除`daemonset`外没有其他pod。

## 恢复调度

```bash
kubectl uncordon node1
```


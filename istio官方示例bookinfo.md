istio版本：1.4.5

## 部署bookinfo

### 部署资源

```bash
kubectl apply -f <(istioctl kube-inject -f samples/bookinfo/platform/kube/bookinfo.yaml)
```

### 确认资源创建

```bash
$ kubectl get services
NAME                       CLUSTER-IP   EXTERNAL-IP   PORT(S)              AGE
details                    10.0.0.31    <none>        9080/TCP             6m
kubernetes                 10.0.0.1     <none>        443/TCP              7d
productpage                10.0.0.120   <none>        9080/TCP             6m
ratings                    10.0.0.15    <none>        9080/TCP             6m
reviews                    10.0.0.170   <none>        9080/TCP             6m
```

**镜像加载很慢**

```bash
$ kubectl get pods
NAME                                        READY     STATUS    RESTARTS   AGE
details-v1-1520924117-48z17                 2/2       Running   0          6m
productpage-v1-560495357-jk1lz              2/2       Running   0          6m
ratings-v1-734492171-rnr5l                  2/2       Running   0          6m
reviews-v1-874083890-f0qf0                  2/2       Running   0          6m
reviews-v2-1343845940-b34q5                 2/2       Running   0          6m
reviews-v3-1813607990-8ch52                 2/2       Running   0          6m
```

### 访问productpage

```bash
$ kubectl exec -it $(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}') -c ratings -- curl productpage:9080/productpage | grep -o "<title>.*</title>"
<title>Simple Bookstore App</title>
```

## 部署bookinfo-gateway

### 部署资源

```bash
$ kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
```

### 确认资源创建

```bash
$ kubectl get gateway
NAME               AGE
bookinfo-gateway   32s
```

### 获取IP端口

```bash
#IP
$ export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
#PORT
$ export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
$ export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
# IP:PORT
$ export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
```

## 访问productpage

### 服务器上访问

```bash
$ curl -s http://${GATEWAY_URL}/productpage | grep -o "<title>.*</title>"
<title>Simple Bookstore App</title>
```

### 浏览器上访问

获取地址

```bash
$ echo $GATEWAY_UR
10.22.19.14:24804
```

打开页面

```http
http://10.22.19.14:24804/productpage
```

**刷新页面，发现评价有时星星是红色的，有时是黑色的，有时没有星星。说明有三个版本在负载均衡。**

![image-20200220183327550](C:\Users\enmonster\AppData\Roaming\Typora\typora-user-images\image-20200220183327550.png)

## 只访问`V1`版本

### 创建`destination rule`

```bash
$ kubectl apply -f samples/bookinfo/networking/destination-rule-all.yaml
$  kubectl get destinationrules.networking.istio.io 
NAME          HOST          AGE
details       details       162m
productpage   productpage   162m
ratings       ratings       162m
reviews       reviews       162m
```

### 创建`V1`版本的`virtual service`

```bash
$ kubectl apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml
```

**此时刷新页面，显示的内容中不包含带星的评价信息。**

![image-20200221140751639](C:\Users\enmonster\AppData\Roaming\Typora\typora-user-images\image-20200221140751639.png)

## 指定用户访问`V2`版本

### 配置`V2`版本的`reviews virtual service`

```bash
$ kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-test-v2.yaml
```

**登录`jason/jason`后，显示的内容变成了黑色星星的评价信息**

![image-20200221141927353](C:\Users\enmonster\AppData\Roaming\Typora\typora-user-images\image-20200221141927353.png)

## 基于权重的流量比例

### 配置`V1`和`V2`权重

```bash
$ kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-80-20.yaml
```

**80%流量到`V1`版本，20%流量到`V2`版本**

## 注入延迟故障

### 配置`ratings virtual service`

```bash
$ kubectl apply -f samples/bookinfo/networking/virtual-service-ratings-test-delay.yaml
```

**普通用户访问正常，jason用户加载评论失败**

![image-20200221151822536](C:\Users\enmonster\AppData\Roaming\Typora\typora-user-images\image-20200221151822536.png)

## 注入500故障

### 配置`ratings virtual service`

```bash
$ kubectl apply -f samples/bookinfo/networking/virtual-service-ratings-test-abort.yaml
```

**普通用户访问正常，jason用户提示`Ratings service is currently unavailable`**

![image-20200221152450937](C:\Users\enmonster\AppData\Roaming\Typora\typora-user-images\image-20200221152450937.png)

## 卸载

```bash
$ samples/bookinfo/platform/kube/cleanup.sh
```

## 参考

https://istio.io/docs/examples/bookinfo/
---
title: 使用同一个deployment实现金丝雀（灰度）发布
date: 2019-02-24 22:29:37
categories:  
 - 容器
tags: 
 - k8s

---


## 一、发布前

发布前，nginx运行三个实例（pod），*副本（replicaset）版本编号855b564c8d*，此时运行状态如下：
```bash
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-hclp9                         1/1       Running            0          12h       172.20.235.210   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
## 二、发布中
### 步骤一：发布

此时进行发布，*新副本版本编号77b6c7b585*，此时发布状态如下：
```bash
nginx-77b6c7b585-vzc9g                         0/1       Running            0          21s       172.20.42.156    10.22.19.15   <none>
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-hclp9                         1/1       Running            0          12h       172.20.235.210   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
### 步骤二：暂停

**执行暂停命令`kubectl rollout pause deployment -n qa nginx`**

等待第一个实例运行成功，此时状态如下：

```bash
nginx-77b6c7b585-vzc9g                         1/1       Running            0          1m        172.20.42.156    10.22.19.15   <none>
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-hclp9                         1/1       Running            0          12h       172.20.235.210   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
>此时为暂停发布状态，三个旧版本实例正常运行，一个新版本实例运行成功，流量将分摊到这四个实例上。

### 步骤三：恢复
**执行恢复命令`kubectl rollout resume deployment -n qa nginx`**

此时状态如下：
```bash
nginx-77b6c7b585-4bt6h                         0/1       Running            0          33s       172.20.190.56    10.22.19.6    <none>
nginx-77b6c7b585-g4wnx                        1/1       Running            0          1m        172.20.42.172    10.22.19.15   <none>
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
>恢复后会执行正常的滚动升级操作，即新增一个新副本实例，删除一个旧副本实例。
>滚动升级时，新旧版本同时存在，总实例最大数量不会超过目标数量的25%，最小数量不会低于目标数量的75%。

## 三、发布完成
实例的副本版本编号都变成77b6c7b585，说明全部升级成功，此时状态如下：
```bash
nginx-77b6c7b585-4bt6h                       1/1       Running            0          36m       172.20.190.56    10.22.19.6    <none>
nginx-77b6c7b585-g4wnx                      1/1       Running            0          36m       172.20.42.172    10.22.19.15   <none>
nginx-77b6c7b585-kcsqm                      1/1       Running            0          2m        172.20.235.253   10.22.19.5    <none>
```

## 四、回滚
发布中发现功能与预期不符，需要取消发布并回滚至上个发布版本，此时可以执行回滚操作。
**执行回滚命令`kubectl rollout undo deployment -n qa nginx`**。
回滚操作在发布中，发布后都可以执行。

### 发布中回滚
当暂停发布进行灰度验证时，发现功能不符合预期，需要取消发布。
暂停时灰度验证状态：
```bash
nginx-77b6c7b585-vzc9g                         1/1       Running            0          2m        172.20.42.156    10.22.19.15   <none>
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-hclp9                         1/1       Running            0          12h       172.20.235.210   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
回滚后状态:
```bash
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-jqbjk                         1/1       Running            0          2m        172.20.235.215   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
>副本编号变成855b564c8d，说明回滚成功，恢复到新版本。

### 发布完成后回滚
全部升级成功后，发现功能异常，回滚到之前版本。
回滚前状态：
```bash
nginx-77b6c7b585-4bt6h                        1/1       Running            0          36m       172.20.190.56    10.22.19.6    <none>
nginx-77b6c7b585-g4wnx                       1/1       Running            0          36m       172.20.42.172    10.22.19.15   <none>
nginx-77b6c7b585-kcsqm                       1/1       Running            0          2m        172.20.235.253   10.22.19.5    <none>
```
回滚后状态：
```bash
nginx-855b564c8d-5qhk7                      1/1       Running            0          6m       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-jqbjk                        1/1       Running            0          6m        172.20.235.215   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                      1/1       Running            0          6m       172.20.35.53     10.22.19.17   <none>
```

## 滚动升级状态
目标实例数3个，旧实例数3个，总实例数3个：
```bash
nginx-855b564c8d-5qhk7                       1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-hclp9                        1/1       Running            0          12h       172.20.235.210   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                       1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
目标实例数3个，旧实例数3个，新实例数1个，总实例数4个：
```bash
nginx-77b6c7b585-vzc9g                        1/1       Running            0          1m        172.20.42.156    10.22.19.15   <none>
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-hclp9                        1/1       Running            0          12h       172.20.235.210   10.22.19.5    <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
目标实例数3个，旧实例数2个，新实例数2个，总实例数4个：
```bash
nginx-77b6c7b585-pmw2f                       1/1       Running            0          15s       172.20.190.51    10.22.19.6    <none>
nginx-77b6c7b585-vzc9g                        1/1       Running            0          3m        172.20.42.156    10.22.19.15   <none>
nginx-855b564c8d-5qhk7                        1/1       Running            0          12h       172.20.199.135   10.22.19.14   <none>
nginx-855b564c8d-n7kn8                        1/1       Running            0          12h       172.20.35.53     10.22.19.17   <none>
```
目标实例数3个，旧实例数1个，新实例数3个，总实例数4个
```bash
nginx-77b6c7b585-4bt6h                        1/1       Running            0          35m       172.20.190.56    10.22.19.6    <none>
nginx-77b6c7b585-g4wnx                       1/1       Running            0          35m       172.20.42.172    10.22.19.15   <none>
nginx-77b6c7b585-kcsqm                       1/1       Running            0          1m        172.20.235.253   10.22.19.5    <none>
nginx-855b564c8d-5qhk7                       1/1       Running            0          13h       172.20.199.135   10.22.19.14   <none>
```
目标实例数3个，新实例数3个，总实例数3个
```bash
nginx-77b6c7b585-4bt6h                        1/1       Running            0          36m       172.20.190.56    10.22.19.6    <none>
nginx-77b6c7b585-g4wnx                       1/1       Running            0          36m       172.20.42.172    10.22.19.15   <none>
nginx-77b6c7b585-kcsqm                       1/1       Running            0          2m        172.20.235.253   10.22.19.5    <none>
```
---
title: CentOS升级内核kernel的几种方式
date: 2019-1-17 20:18:01
categories:  
 - linux
tags: 
 - kernel

---

## 小版本升级

```bash
# install
yum install kernel* -y
# reboot
init 6
```

## 安装最新稳定版内核

```bash
# import key
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
# install elrepo repo
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
# list kernel
yum --disablerepo=\* --enablerepo=elrepo-kernel list kernel*
# install kernel
yum --enablerepo=elrepo-kernel install kernel-ml-devel kernel-ml -y
# yum --disablerepo=\* --enablerepo=elrepo-kernel install -y kernel-ml.x86_64
# modify grub
grub2-set-default 0
grub2-mkconfig -o /boot/grub2/grub.cfg
# reboot
reboot
```

## 安装指定版本内核

推荐一个可以找到各个版本内核的国内镜像站：http://mirror.rc.usf.edu/compute_lock/elrepo/kernel/el7/x86_64/RPMS

本次安装以4.19版本的内核示例：

```bash
# install
rpm -ivh http://mirror.rc.usf.edu/compute_lock/elrepo/kernel/el7/x86_64/RPMS/kernel-ml-4.19.12-1.el7.elrepo.x86_64.rpm
# modify grub
sed -i "s/GRUB_DEFAULT=saved/GRUB_DEFAULT=0/" /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg
# reboot
reboot
```



## 编译源码安装

没试过

## 参考

https://mritd.me/2016/11/08/update-centos-kernel/
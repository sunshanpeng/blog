### 下载资源文件

```bash
git clone https://github.com/coreos/prometheus-operator.git
```

### 创建单独的命名空间

```bash
kubectl create namespace monitoring
```

### 替换默认命名空间并创建operator

```bash
sed "s|namespace: default|namespace: monitoring|g" bundle.yaml | kubectl apply -n monitoring -f -
```

### 查看Prometheus Operator部署状态，确保已正常运行

```bash
kubectl -n monitoring get pods
NAME                                   READY     STATUS    RESTARTS   AGE
prometheus-operator-6db8dbb7dd-2hz55   1/1       Running   0          19s
```

### 创建example-app

内容如下

```yaml
kind: Service
apiVersion: v1
metadata:
  name: example-app
  labels:
    app: example-app
spec:
  selector:
    app: example-app
  ports:
  - name: web
    port: 8080
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: example-app
spec:
  replicas: 3
  template:
    metadata:
      labels:
        app: example-app
    spec:
      containers:
      - name: example-app
        image: fabxc/instrumented_app
        ports:
        - name: web
          containerPort: 8080
```

```bash
#创建
kubectl apply -f example-app.yaml
#查看
kubectl get pods
NAME                        READY     STATUS    RESTARTS   AGE
example-app-94c8bc8-l27vx   2/2       Running   0          1m
example-app-94c8bc8-lcsrm   2/2       Running   0          1m
example-app-94c8bc8-n6wp5   2/2       Running   0          1m
```

### 创建service-monitor

内容如下：

```yaml
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: example-app
  namespace: monitoring
  labels:
    team: frontend
spec:
  namespaceSelector:
    matchNames:
    - default
  selector:
    matchLabels:
      app: example-app
  endpoints:
  - port: web
```

```bash
 kubectl create -f example-app-service-monitor.yaml
```

### 创建prometheus-rbac

内容如下：

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: prometheus
  namespace: monitoring
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: prometheus
rules:
- apiGroups: [""]
  resources:
  - nodes
  - services
  - endpoints
  - pods
  verbs: ["get", "list", "watch"]
- apiGroups: [""]
  resources:
  - configmaps
  verbs: ["get"]
- nonResourceURLs: ["/metrics"]
  verbs: ["get"]
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: prometheus
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: prometheus
subjects:
- kind: ServiceAccount
  name: prometheus
  namespace: monitoring
```

```bash
 kubectl -n monitoring create -f prometheus-rbac.yaml
```

### 创建告警规则

内容如下：

```yaml
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    prometheus: example
    role: alert-rules
  name: prometheus-example-rules
spec:
  groups:
  - name: ./example.rules
    rules:
    - alert: ExampleAlert
      expr: vector(1)
```

```bash
kubectl -n monitoring create -f example-rule.yaml
```

### 创建prometheus实例

内容如下：

```yaml
apiVersion: monitoring.coreos.com/v1
kind: Prometheus
metadata:
  name: inst
  namespace: monitoring
spec:
  serviceAccountName: prometheus
  serviceMonitorSelector:
    matchLabels:
      team: frontend
  ruleSelector:
    matchLabels:
      role: alert-rules
      prometheus: example
  alerting:
    alertmanagers:
    - name: alertmanager-example
      namespace: monitoring
      port: web
  resources:
    requests:
      memory: 400Mi
```

```bash
kubectl -n monitoring apply -f prometheus-inst.yaml
```

### 通过`port-forward`访问prometheus

```bash
kubectl -n monitoring port-forward statefulsets/prometheus-inst 9090:9090 --address 0.0.0.0
```

输入<http://localhost:9090/>即可访问。

### 创建alertmanager配置

内容如下：

```yaml
global:
  resolve_timeout: 5m
route:
  group_by: ['job']
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 12h
  receiver: 'webhook'
receivers:
- name: 'webhook'
  webhook_configs:
  - url: 'http://alertmanagerwh:30500/'
```

```bash
kubectl -n monitoring create secret generic alertmanager-inst --from-file=alertmanager.yaml
```

### 创建alertmanager实例

内容如下：

```yaml
apiVersion: monitoring.coreos.com/v1
kind: Alertmanager
metadata:
  name: inst
  namespace: monitoring
spec:
  replicas: 3
```

```bash
kubectl -n monitoring apply -f alertmanager-inst.yaml
```

### 通过`port-forward`访问alertmanager

```bash
kubectl -n monitoring port-forward statefulsets/alertmanager-inst 9093:9093 --address 0.0.0.0
```


istio版本：1.4.5

k8s版本：1.14.8

docker版本：18.6.1

内核版本：4.19.12-1.el7.elrepo.x86_64

## 安装

### 下载安装包

#### 方式一：在线下载

```bash
curl -L https://istio.io/downloadIstio | sh -
```

#### 方式二：离线下载

https://github.com/istio/istio/releases

下载安装包后上传到服务器并解压

### 进入安装包并设置环境变量

```sh
$ cd istio-1.4.5
# istioctl在/bin目录下
# K8S安装yaml文件在/install/kubernetes目录下
# 示例程序在/samples下
$ ll
total 28
drwxr-x---  2 root root    21 Feb 14 01:23 bin
drwxr-xr-x  6 root root    74 Feb 14 01:23 install
-rw-r--r--  1 root root 11348 Feb 14 01:23 LICENSE
-rw-r-----  1 root root   657 Feb 14 01:23 manifest.yaml
-rw-r--r--  1 root root  6080 Feb 14 01:23 README.md
drwxr-xr-x 19 root root  4096 Feb 14 01:23 samples
drwxr-x---  3 root root   132 Feb 14 01:23 tools

$ export PATH=$PWD/bin:$PATH
```

### 安装istio

#### 安装demo示例（不适用生产环境）

```bash
$ istioctl manifest apply --set profile=demo
- Applying manifest for component Base...
✔ Finished applying manifest for component Base.
- Applying manifest for component Tracing...
- Applying manifest for component Policy...
- Applying manifest for component Galley...
- Applying manifest for component Citadel...
- Applying manifest for component EgressGateway...
- Applying manifest for component Pilot...
- Applying manifest for component Telemetry...
- Applying manifest for component Injector...
- Applying manifest for component IngressGateway...
- Applying manifest for component Prometheus...
- Applying manifest for component Kiali...
- Applying manifest for component Grafana...
✔ Finished applying manifest for component Injector.
✔ Finished applying manifest for component Galley.
✔ Finished applying manifest for component Citadel.
✔ Finished applying manifest for component Prometheus.
✔ Finished applying manifest for component IngressGateway.
✔ Finished applying manifest for component Kiali.
✔ Finished applying manifest for component Pilot.
✔ Finished applying manifest for component Policy.
✔ Finished applying manifest for component Tracing.
✔ Finished applying manifest for component EgressGateway.
✔ Finished applying manifest for component Grafana.
✔ Finished applying manifest for component Telemetry.


✔ Installation complete
```

#### 查看安装情况

```bash
$ kubectl get svc -n istio-system
NAME                     TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                                                                                                                      AGE
grafana                  ClusterIP      172.21.92.238    <none>        3000/TCP                                                                                                                     117s
istio-citadel            ClusterIP      172.21.219.100   <none>        8060/TCP,15014/TCP                                                                                                           119s
istio-egressgateway      ClusterIP      172.21.132.59    <none>        80/TCP,443/TCP,15443/TCP                                                                                                     117s
istio-galley             ClusterIP      172.21.181.69    <none>        443/TCP,15014/TCP,9901/TCP,15019/TCP                                                                                         119s
istio-ingressgateway     LoadBalancer   172.21.147.170   <pending>     15020:34070/TCP,80:24804/TCP,443:38932/TCP,15029:23713/TCP,15030:30635/TCP,15031:34451/TCP,15032:30379/TCP,15443:39703/TCP   118s
istio-pilot              ClusterIP      172.21.121.250   <none>        15010/TCP,15011/TCP,8080/TCP,15014/TCP                                                                                       118s
istio-policy             ClusterIP      172.21.178.10    <none>        9091/TCP,15004/TCP,15014/TCP                                                                                                 117s
istio-sidecar-injector   ClusterIP      172.21.223.149   <none>        443/TCP                                                                                                                      119s
istio-telemetry          ClusterIP      172.21.49.247    <none>        9091/TCP,15004/TCP,15014/TCP,42422/TCP                                                                                       116s
jaeger-agent             ClusterIP      None             <none>        5775/UDP,6831/UDP,6832/UDP                                                                                                   2m
jaeger-collector         ClusterIP      172.21.159.105   <none>        14267/TCP,14268/TCP,14250/TCP                                                                                                119s
jaeger-query             ClusterIP      172.21.203.16    <none>        16686/TCP                                                                                                                    118s
kiali                    ClusterIP      172.21.207.47    <none>        20001/TCP                                                                                                                    118s
prometheus               ClusterIP      172.21.71.218    <none>        9090/TCP                                                                                                                     118s
tracing                  ClusterIP      172.21.49.79     <none>        80/TCP                                                                                                                       118s
zipkin                   ClusterIP      172.21.13.237    <none>        9411/TCP                                                                                                                     117s
```

**镜像下载速度很慢，需要加速**

> 使用指定仓库
> ```bash
> istioctl manifest apply --set hub=harbor.enmonster.com/istio
> ```

```bash
$ kubectl get pods -n istio-system
grafana-5f798469fd-hm5vs                  1/1     Running   0          142m
istio-citadel-58bb67f9b8-bkwkm            1/1     Running   0          142m
istio-egressgateway-6fd57475b5-wdqck      1/1     Running   0          142m
istio-galley-7d4b9874c8-jk495             1/1     Running   0          142m
istio-ingressgateway-7d65bf7fdf-jbqst     1/1     Running   0          142m
istio-pilot-65f8557545-kbs75              1/1     Running   0          142m
istio-policy-6c6449c56f-btzpd             1/1     Running   0          142m
istio-sidecar-injector-774969d686-lgnjb   1/1     Running   0          142m
istio-telemetry-585cc965f7-vjwwn          1/1     Running   8          142m
istio-tracing-cd67ddf8-wpdqr              1/1     Running   0          142m
kiali-7964898d8c-4z88n                    1/1     Running   0          142m
prometheus-586d4445c7-wv9qv               1/1     Running   0          142m
```

### 注入Sidecar

#### 方式一：指定namespace自动注入

```bash
$ kubectl label namespace <namespace> istio-injection=enabled
```

#### 方式二：指定pod注入

```bash
#三种方式
$ istioctl kube-inject -f resource.yaml | kubectl apply -f -
$ kubectl apply -f <(istioctl kube-inject -f <resource.yaml>)
$ kubectl get deployment deploy -o yaml | istioctl kube-inject -f - | kubectl apply -f -
```

istioctl kube-inject 命令用于在创建部署之前修改yaml 文件，把 Envoy 注入到 Kubernetes 资源。

## 卸载

```bash
$ istioctl manifest generate --set profile=demo | kubectl delete -f -
```

## 参考

https://istio.io/docs/setup/getting-started/
istio版本：1.4.5


### 安装bash-completion

#### ubuntu

```bash
$ apt-get install bash-completion
```

#### centos

```bash
$ yum install bash-completion
```

### 配置`~/.bash_profile`

```bash
$ echo '[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"' >> ~/.bash_profile
```

### 启用`istioctl.bash`

```bash
$ cp istio-1.4.5/tools/istioctl.bash ~/istioctl.bash
$ source ~/istioctl.bash
$ echo 'source ~/istioctl.bash' >> .bashrc
$ source .bashrc
```

### 验证

```bash
$ istioctl proxy-<TAB>
proxy-config proxy-status
```

### 参考

https://istio.io/docs/ops/diagnostic-tools/istioctl/#enabling-auto-completion
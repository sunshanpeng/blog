## 版本

istio:1.5.0

Kubernetes: 1.14.8

## 前言

安装istio的`default`或者`demo`模式，会默认安装一个`istio-ingressgateway`，但是实际业务中可能需要多个网关，此时需要自己安装。

思路是创建多套网关需要的资源：

**必选项：**

- Role
- RoleBinding
- ServiceAccount
- Service
- Deployment
- Gateway

**可选项：**

- PodDisruptionBudget
- HorizontalPodAutoscaler

这些资源可以用从已经部署的kubernetes资源中获取，也可以从istioctl工具中获取。

## 获取资源

两种方式选择一种即可

### kubernetes

```bash
$ kubectl get deployments -n istio-system istio-ingressgateway -o yaml > istio-ingressgateway-deploy.yaml
$ kubectl get svc -n istio-system istio-ingressgateway -o yaml > istio-ingressgateway-service.yaml
$ kubectl get gateway -n istio-system ingressgateway -o yaml > istio-ingressgateway-gateway.yaml
$ kubectl get sa -n istio-system istio-ingressgateway-service-account -o yaml > istio-ingressgateway-service-account.yaml
$ kubectl get role -n istio-system istio-ingressgateway-sds -o yaml > istio-ingressgateway-role.yaml
$ kubectl get rolebinding -n istio-system istio-ingressgateway-sds -o yaml > istio-ingressgateway-rolebinding.yaml
```

### istioctl

```bash
$ istioctl manifest generate > manifest.yaml
```

生成的文件为完整的istio资源，找到IngressGateway相关的资源。

## 替换资源

修改资源文件中的`istio-ingressgateway`和`ingressgateway`为自己定义的名字，比如`internal-gateway`。

然后用`kubectl apply -f file`即可。

## 完整部署资源

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: internal-gateway-sds
  namespace: istio-system
  labels:
    release: istio
rules:
- apiGroups: [""]
  resources: ["secrets"]
  verbs: ["get", "watch", "list"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: internal-gateway-sds
  namespace: istio-system
  labels:
    release: istio
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: internal-gateway-sds
subjects:
- kind: ServiceAccount
  name: internal-gateway-service-account

---

apiVersion: v1
kind: Service
metadata:
  name: internal-gateway
  namespace: istio-system
  labels:
    app: internal-gateway
    istio: internal-gateway
    release: istio
spec:
  type: LoadBalancer
  selector:
    app: internal-gateway
    istio: internal-gateway
  ports:
    -
      name: status-port
      port: 15020
      targetPort: 15020
    -
      name: http2
      port: 80
      targetPort: 80
    -
      name: https
      port: 443
    -
      name: kiali
      port: 15029
      targetPort: 15029
    -
      name: prometheus
      port: 15030
      targetPort: 15030
    -
      name: grafana
      port: 15031
      targetPort: 15031
    -
      name: tracing
      port: 15032
      targetPort: 15032
    -
      name: tls
      port: 15443
      targetPort: 15443

---

apiVersion: v1
kind: ServiceAccount
metadata:
  name: internal-gateway-service-account
  namespace: istio-system
  labels:
    app: internal-gateway
    istio: internal-gateway
    release: istio

---

apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
  labels:
    app: internal-gateway
    istio: internal-gateway
    operator.istio.io/component: IngressGateways
    operator.istio.io/managed: Reconcile
    operator.istio.io/version: 1.5.0
    release: istio
  name: internal-gateway
  namespace: istio-system
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: internal-gateway
      istio: internal-gateway
  strategy:
    rollingUpdate:
      maxSurge: 100%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      annotations:
        sidecar.istio.io/inject: "false"
      creationTimestamp: null
      labels:
        app: internal-gateway
        chart: gateways
        heritage: Tiller
        istio: internal-gateway
        release: istio
        service.istio.io/canonical-name: istio-ingressgateway
        service.istio.io/canonical-revision: "1.5"
    spec:
      affinity:
        nodeAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - preference:
              matchExpressions:
              - key: beta.kubernetes.io/arch
                operator: In
                values:
                - amd64
            weight: 2
          - preference:
              matchExpressions:
              - key: beta.kubernetes.io/arch
                operator: In
                values:
                - ppc64le
            weight: 2
          - preference:
              matchExpressions:
              - key: beta.kubernetes.io/arch
                operator: In
                values:
                - s390x
            weight: 2
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: beta.kubernetes.io/arch
                operator: In
                values:
                - amd64
                - ppc64le
                - s390x
      containers:
      - args:
        - proxy
        - router
        - --domain
        - $(POD_NAMESPACE).svc.cluster.local
        - --proxyLogLevel=warning
        - --proxyComponentLogLevel=misc:error
        - --log_output_level=default:info
        - --drainDuration
        - 45s
        - --parentShutdownDuration
        - 1m0s
        - --connectTimeout
        - 10s
        - --serviceCluster
        - internal-gateway
        - --zipkinAddress
        - zipkin.istio-system:9411
        - --proxyAdminPort
        - "15000"
        - --statusPort
        - "15020"
        - --controlPlaneAuthPolicy
        - NONE
        - --discoveryAddress
        - istio-pilot.istio-system.svc:15012
        - --trust-domain=cluster.local
        env:
        - name: JWT_POLICY
          value: first-party-jwt
        - name: PILOT_CERT_PROVIDER
          value: istiod
        - name: ISTIO_META_USER_SDS
          value: "true"
        - name: CA_ADDR
          value: istio-pilot.istio-system.svc:15012
        - name: NODE_NAME
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: spec.nodeName
        - name: POD_NAME
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
        - name: INSTANCE_IP
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: status.podIP
        - name: HOST_IP
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: status.hostIP
        - name: SERVICE_ACCOUNT
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: spec.serviceAccountName
        - name: ISTIO_META_WORKLOAD_NAME
          value: internal-gateway
        - name: ISTIO_META_OWNER
          value: kubernetes://apis/apps/v1/namespaces/istio-system/deployments/internal-gateway
        - name: ISTIO_META_MESH_ID
          value: cluster.local
        - name: ISTIO_AUTO_MTLS_ENABLED
          value: "true"
        - name: ISTIO_META_POD_NAME
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.name
        - name: ISTIO_META_CONFIG_NAMESPACE
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
        - name: ISTIO_META_ROUTER_MODE
          value: sni-dnat
        - name: ISTIO_META_CLUSTER_ID
          value: Kubernetes
        image: docker.io/istio/proxyv2:1.5.0
        imagePullPolicy: IfNotPresent
        name: istio-proxy
        ports:
        - containerPort: 15020
          protocol: TCP
        - containerPort: 80
          protocol: TCP
        - containerPort: 443
          protocol: TCP
        - containerPort: 15029
          protocol: TCP
        - containerPort: 15030
          protocol: TCP
        - containerPort: 15031
          protocol: TCP
        - containerPort: 15032
          protocol: TCP
        - containerPort: 15443
          protocol: TCP
        - containerPort: 15011
          protocol: TCP
        - containerPort: 8060
          protocol: TCP
        - containerPort: 853
          protocol: TCP
        - containerPort: 15090
          name: http-envoy-prom
          protocol: TCP
        readinessProbe:
          failureThreshold: 30
          httpGet:
            path: /healthz/ready
            port: 15020
            scheme: HTTP
          initialDelaySeconds: 1
          periodSeconds: 2
          successThreshold: 1
          timeoutSeconds: 1
        resources:
          limits:
            cpu: "2"
            memory: 1Gi
          requests:
            cpu: 100m
            memory: 128Mi
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /var/run/secrets/istio
          name: istiod-ca-cert
        - mountPath: /var/run/ingress_gateway
          name: ingressgatewaysdsudspath
        - mountPath: /etc/istio/pod
          name: podinfo
        - mountPath: /etc/istio/ingressgateway-certs
          name: ingressgateway-certs
          readOnly: true
        - mountPath: /etc/istio/ingressgateway-ca-certs
          name: ingressgateway-ca-certs
          readOnly: true
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      serviceAccount: internal-gateway-service-account
      serviceAccountName: internal-gateway-service-account
      terminationGracePeriodSeconds: 30
      volumes:
      - configMap:
          defaultMode: 420
          name: istio-ca-root-cert
        name: istiod-ca-cert
      - downwardAPI:
          defaultMode: 420
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.labels
            path: labels
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.annotations
            path: annotations
        name: podinfo
      - emptyDir: {}
        name: ingressgatewaysdsudspath
      - name: ingressgateway-certs
        secret:
          defaultMode: 420
          optional: true
          secretName: istio-ingressgateway-certs
      - name: ingressgateway-ca-certs
        secret:
          defaultMode: 420
          optional: true
          secretName: istio-ingressgateway-ca-certs
          
---

apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: internal-gateway
spec:
  selector:
    istio: internal-gateway
  servers:
    - port:
        number: 80
        name: http
        protocol: HTTP
      hosts:
        - '*'
```

## 测试资源

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    name: http
  selector:
    app: nginx
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 80
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: nginx-1
spec:
  hosts:
  - "*"
  gateways:
  - internal-gateway
  http:
  - route:
    - destination:
        host: nginx
        port:
          number: 80
```

## 测试

```bash
$ GATEWAY_HOST=$(kubectl get po -l istio=internal-gateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
$ GATEWAY_PORT=$(kubectl get svc -n istio-system internal-gateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
$ curl $GATEWAY_HOST:$GATEWAY_PORT
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

## 参考

https://istio.io/docs/tasks/traffic-management/ingress/ingress-control/#determining-the-ingress-ip-and-ports

https://www.learncloudnative.com/blog/2020-01-09-deploying_multiple_gateways_with_istio/
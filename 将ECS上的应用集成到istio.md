

## 网格拓展Mesh Expansion

Mesh Expansion是指部署在Kubernetes之中的Istio服务网格提供的一种将虚拟机或物理裸机集成进入到服务网格的方法。

Mesh Expansion对于用户从遗留系统往云上迁移过程中有着非常重要的作用，在微服务体系结构中，无法要求所有的工作负载都在Kubernetes中运行，用户的一些应用程序可能在Kubernetes中运维，而另外一些可能在虚拟机或物理裸机中运行。

通过一套Istio控制面板就可以管理跨Kubernetes与虚拟机或物理裸机的若干服务。这样既能保证原有业务的正常运转，又能实现Kubernetes与虚拟机上的应用服务混合编排的能力。

![](https://static-aliyun-doc.oss-cn-hangzhou.aliyuncs.com/assets/img/zh-CN/5985637751/p12682.png)

如图所示，`details`组件和数据库运行在Kubernetes之外的ECS上。

把一个VM等同于一个Pod，部署业务服务和Sidecar，然后交给istio编排。

## 参考

https://help.aliyun.com/document_detail/90707.html

https://istio.io/docs/examples/virtual-machines/single-network/

https://istio.io/docs/examples/virtual-machines/multi-network/
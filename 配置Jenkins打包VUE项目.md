## 一、下载NodeJS插件

### 进入插件管理

![](http://image.sunshanpeng.com/201908301902_419.png)

### 在可选插件里面安装NodeJS

![](http://image.sunshanpeng.com/201908301905_538.png)

### 等待安装完成

![](http://image.sunshanpeng.com/201908301906_385.png)

## 二、配置NodeJS插件

### 进入全局工具配置

![](http://image.sunshanpeng.com/201908301908_350.png)

### 新增NodeJS

![](http://image.sunshanpeng.com/201908301912_783.png)

## 三、配置Job

### 配置执行shell

![](http://image.sunshanpeng.com/201908301910_887.png)

shell命令：

```shell
# 打包
node -v
npm -v 
npm install chromedriver --chromedriver_cdnurl=http://cdn.npm.taobao.org/dist/chromedriver
npm install
rm -rf dist
npm run build
```

cnpm：

```shell
# 打包
node -v
npm -v 
npm install -g cnpm --registry=https://registry.npm.taobao.org
#cnpm install rimraf -g
#rimraf node_modules
cnpm install
rm -rf dist
npm run build
```

升级Vue：

```bash
# 打包
node -v
npm -v 
npm install chromedriver --chromedriver_cdnurl=http://cdn.npm.taobao.org/dist/chromedriver
npm install -g @vue/cli
cnpm install -s vue-template-compiler
vue -V
rm -rf node_modules
cnpm install
rm -rf dist
npm run build
```


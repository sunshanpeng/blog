---
title: 常用的IntelliJ IDEA 插件推荐
date: 2019-02-24 22:29:37
categories:  
 - java
tags: 
 - idea

---

### 一、Free Mybatis plugin

> <https://plugins.jetbrains.com/plugin/8321-free-mybatis-plugin/>

1.把mybatis的xml文件和代码关联上，可以在mapper接口直接跳转到对应的SQL语句。

![mybatis](http://image.sunshanpeng.com/201908181216_569.png)

2.如果没有对应的SQL语句，可以快捷补全（当然具体SQL语句还是要自己写）。

![sql](http://image.sunshanpeng.com/201908181217_431.png)

### 二、Maven Helper

> <https://plugins.jetbrains.com/plugin/7179-maven-helper/>

简单、方便的查看maven依赖和冲突

![maven helper](http://image.sunshanpeng.com/201908181217_88.png)

![maven helper](http://image.sunshanpeng.com/201908181218_140.png)

### 三、GenerateAllSetter

> <https://plugins.jetbrains.com/plugin/9360-generateallsetter/>

一键调用一个对象的所有的set方法

![generateAllSetter](http://image.sunshanpeng.com/201908181219_563.png)

### 四、RestfulToolkit

> <https://plugins.jetbrains.com/plugin/10292-restfultoolkit/>

通过URL查询对应的方法，可以根据请求方法过滤，另外支持简单的HTTP调用。

![restfulToolkit](http://image.sunshanpeng.com/201908181219_755.png)

![](http://image.sunshanpeng.com/201908181220_291.png)

### 五、Key Promoter X

> <https://plugins.jetbrains.com/plugin/9792-key-promoter-x/>

帮助我们快速的掌握 **IntelliJ IDEA**的快捷键。

![](http://image.sunshanpeng.com/201908181221_36.gif)

### 六、GsonFormat

> <https://plugins.jetbrains.com/plugin/7654-gsonformat/>

快速方便的把json字符串转换为实体类（快捷键ALT+S）。

![](http://image.sunshanpeng.com/201908181221_537.png)

### 七、String Manipulation

> <https://plugins.jetbrains.com/plugin/2162-string-manipulation/>

快捷方便的把字符串在驼峰、大小写等格式之间转换（快捷键Alt+M)。

![](http://image.sunshanpeng.com/201908181222_332.png)

![Screenshot 2](https://plugins.jetbrains.com/files/2162/screenshot_16015.png)

### 八、Alibaba Java Coding Guidelines

> <https://plugins.jetbrains.com/plugin/10046-alibaba-java-coding-guidelines/>

扫描代码是否符合规范

![](http://image.sunshanpeng.com/201908181222_445.png)

### 九、Lombok

> https://plugins.jetbrains.com/plugin/6317-lombok/

简化java bean的代码量，自动生成get set toString EqualsAndHashCode 构造器。

> 需要引入依赖包
>
> ```
> <dependency>  
>           <groupId>org.projectlombok</groupId>  
>           <artifactId>lombok</artifactId>   
> </dependency>
> ```

### 十、.ignore

> <https://plugins.jetbrains.com/plugin/7495--ignore/>

使用协作或者打包工具时，方便的处理要忽略的文件和文件夹。

![Screenshot 1](https://plugins.jetbrains.com/files/7495/screenshot_14958.png)

### 十一、Vue.js

> https://plugins.jetbrains.com/plugin/9442-vue-js/

用IDEA开发Vue的插件。

### 十二、element

> https://plugins.jetbrains.com/plugin/10524-element/

饿了么开源前端框架element的插件。

![element](https://plugins.jetbrains.com/files/10524/screenshot_17918.png)
---
title: Windows电脑本地开发时连接Apollo配置中心启动慢
date: 2019-03-24 22:26:37
categories:  
 - java
tags: 
 - apollo

---

### 表现：

启动时中间停顿了10秒左右

![start](http://image.sunshanpeng.com/201908181151_8.png)

### 原因：

电脑上网卡太多

![ip](http://image.sunshanpeng.com/201908181156_911.png)

![](http://image.sunshanpeng.com/201908181158_629.png)

### 解决方法：

1. 禁用多余网卡

2. 在启动参数指定本机IP：`-Dhost.ip=10.208.202.251`
> 10.208.202.251改成自己的IP地址

### 原理：

指定本机IP后就会跳过查找有效IP这一步

### 具体代码

```java
package com.ctrip.framework.foundation.internals;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

public enum NetworkInterfaceManager {
  INSTANCE;

  private InetAddress m_local;

  private InetAddress m_localHost;

  private NetworkInterfaceManager() {
    load();
  }

  public InetAddress findValidateIp(List<InetAddress> addresses) {
    InetAddress local = null;
    int maxWeight = -1;
    for (InetAddress address : addresses) {
      if (address instanceof Inet4Address) {
        int weight = 0;

        if (address.isSiteLocalAddress()) {
          weight += 8;
        }

        if (address.isLinkLocalAddress()) {
          weight += 4;
        }

        if (address.isLoopbackAddress()) {
          weight += 2;
        }

        // has host name
        // TODO fix performance issue when calling getHostName
        if (!Objects.equals(address.getHostName(), address.getHostAddress())) {
          weight += 1;
        }

        if (weight > maxWeight) {
          maxWeight = weight;
          local = address;
        }
      }
    }
    return local;
  }

  public String getLocalHostAddress() {
    return m_local.getHostAddress();
  }

  public String getLocalHostName() {
    try {
      if (null == m_localHost) {
        m_localHost = InetAddress.getLocalHost();
      }
      return m_localHost.getHostName();
    } catch (UnknownHostException e) {
      return m_local.getHostName();
    }
  }

  private String getProperty(String name) {
    String value = null;

    value = System.getProperty(name);

    if (value == null) {
      value = System.getenv(name);
    }

    return value;
  }

  private void load() {
    String ip = getProperty("host.ip");//除了启动参数也可以在环境变量中指定host.ip

    if (ip != null) {//如果指定了host.ip就直接拿指定IP的网卡信息
      try {
        m_local = InetAddress.getByName(ip);
        return;
      } catch (Exception e) {
        System.err.println(e);
        // ignore
      }
    }

    try {
      Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
      List<NetworkInterface> nis = interfaces == null ? Collections.<NetworkInterface>emptyList() : Collections.list(interfaces);
      List<InetAddress> addresses = new ArrayList<InetAddress>();
      InetAddress local = null;

      try {
        for (NetworkInterface ni : nis) {
          if (ni.isUp() && !ni.isLoopback()) {
            addresses.addAll(Collections.list(ni.getInetAddresses()));
          }
        }
        local = findValidateIp(addresses);
      } catch (Exception e) {
        // ignore
      }
      if (local != null) {
        m_local = local;
        return;
      }
    } catch (SocketException e) {
      // ignore it
    }

    m_local = InetAddress.getLoopbackAddress();
  }
}
```



注：

- 官方说还可以通过改host的方式实现，但是我在win10电脑上没有成功。
- 1.4修复了该问题

参考链接：[https://github.com/ctripcorp/apollo/wiki/%E9%83%A8%E7%BD%B2&%E5%BC%80%E5%8F%91%E9%81%87%E5%88%B0%E7%9A%84%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98#6-%E5%AE%A2%E6%88%B7%E7%AB%AF%E5%A4%9A%E5%9D%97%E7%BD%91%E5%8D%A1%E9%80%A0%E6%88%90%E8%8E%B7%E5%8F%96ip%E4%B8%8D%E5%87%86%E5%A6%82%E4%BD%95%E8%A7%A3%E5%86%B3](https://github.com/ctripcorp/apollo/wiki/部署&开发遇到的常见问题#6-客户端多块网卡造成获取ip不准如何解决)

https://github.com/ctripcorp/apollo/issues/1457
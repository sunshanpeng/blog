---
title: Pod的意义
date: 2019-02-24 22:29:37
categories:  
 - 容器
tags: 
 - k8s

---

Kubernetes 使用 Pod 来管理容器，每个 Pod 可以包含一个或多个紧密关联的容器。

Pod 是一组紧密关联的容器集合，它们共享 PID、IPC、Network 和 UTS namespace，是 Kubernetes 调度的基本单位。

Pod 内的多个容器共享网络和文件系统，可以通过进程间通信和文件共享这种简单高效的方式组合完成服务。

![img](http://image.sunshanpeng.com/assets%252F-LDAOok5ngY4pc1lEDes%252F-LM_rqip-tinVoiFZE0I%252F-LM_sIwx4E3-69Ml-jNW%252Fpod.png)



## Pod的意义

容器都是单进程运行的，不是因为容器只能运行一个进程，而是容器没有管理多个进程的能力。

容器里PID=1的进程就是应用本身，其他的进程都是这个PID=1进程的子进程。如果启动了第二个进程，那这第二个进程异常退出的时候，外部并不能感知，也就管理不了。

在一个真正的操作系统里，进程并不是“孤苦伶仃”地独自运行的，应用之间可能有着密切的协作关系，必须部署在同一台机器上。

这些密切关系包括但不限于：互相之间会发生直接的文件交换，使用localhost或者Socket文件进行本地通信，会发生非常频繁的远程调用，共享某些Linux Namespace。



## Pod的实现原理

首先，Pod只是一个逻辑概念，是一组共享了某些资源的容器。

Kubernetes真正处理的，还是宿主机操作系统上Linux容器的Namespace和Cgroups，而并不存在一个所谓的Pod的边界或者隔离环境。

Kubernetes里，Pod的实现需要使用一个中间容器，叫做Infra容器，在Pod中，Infra容器永远都是第一个被创建的容器。

其他用户自定义的容器，通过Join Network Namespace的方式与Infra容器关联在一起。

![img](http://image.sunshanpeng.com/image2018-10-17_19-22-48.png)

如图所示，这个Pod里除了两个用户容器，还有一个Infra容器。

这个Infra容器占用极少的资源，使用了一个非常特殊的镜像：k8s.gcr.io/pause，这个镜像永远处于“暂停”状态，100-200KB左右大小。

Infra容器生成NetWork Namespace后，用户容器加入Infra容器的Network Namespace中，用户容器和Infra容器在宿主机上的Namespace文件是一样的。

这意味着，对Pod中的容器A和容器B来说：

- 它们可以直接使用localhost进行通信；
- 他们看到的网络设备和Infra容器的一样；
- 一个Pod只有一个IP地址，也就是容器A、容器B、Infra容器的ip地址一致；
- 所有的网络资源被改Pod中的所有容器共享；
- Pod的生命周期只跟Infra容器一致，与容器A和B无关。

对于同一个Pod里面的所有用户容器来说，它们的进出流量，都是通过Infra容器完成的。



Pod的本质，实际上是在扮演传统基础设施“虚拟机”的角色；容器则是这个虚拟机里运行的用户进程。
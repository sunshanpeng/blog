## 示例

请求示例

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts: #适用于用户可寻址的目的地,是客户端向服务发送请求时使用的地址
  - reviews
  - www.baidu.com
  - *.google.com #可以使用通配符（“*”）前缀，从而为所有匹配的服务创建一套路由规则
  - xxxxxx #可以建一条没有可路由条目的记录
  http:
   - fault: #故障注入
      delay: #延迟故障
        percentage:
          value: 0.1 #百分比，0.1%
        fixedDelay: 5s #固定延迟5S
      abort:
        httpStatus: 500 #HTTP错误状态码
        percentage:
          value: 1 #百分比，1%
  - match: #可以匹配uri、scheme、method、authority、headers、port、sourceLabels、queryParams
    - headers:
        end-user:
          exact: jason
    route: #多条match规则都要满足，是与的关系
    - destination:
        host: reviews #destinationRule资源名称
        subset: v2 #destinationRule服务子集，版本的意思
  - route: #路由规则从上到下按顺序评估，一般最后定义一个默认规则
    - destination:
        host: reviews
        subset: v3
    timeout: 10s #指定服务子集的超时时间，默认15S
    retries: #重试配置
      attempts: 3 #重试次数
      perTryTimeout: 2s #每次重试超时时间
      retryOn: gateway-error,connect-failure,refused-stream
    mirror: #流量镜像
      host: httpbin
      subset: v2
    mirror_percent: 100 #镜像比例，100%
```

uri示例

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: bookinfo
spec:
  hosts:
    - bookinfo.com
  http:
  - match:
    - uri:
        prefix: /reviews
    route:
    - destination:
        host: reviews
  - match:
    - uri:
        prefix: /ratings
    route:
    - destination:
        host: ratings
    - uri:
        prefix: "/oldcatalog"
    rewrite:
      uri: "/newcatalog"
```

百分比权重示例

```yaml
spec:
  hosts:
  - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v1
      weight: 75
    - destination:
        host: reviews
        subset: v2
      weight: 25
```


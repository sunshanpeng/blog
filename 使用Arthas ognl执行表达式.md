## Arthas ognl命令介绍

>  https://alibaba.github.io/arthas/ognl.html 

## 使用方法

命令格式：

```bash
ognl 'express'
```

 ### 调用静态函数

```bash
$ ognl '@System@getProperty("java.home")'
@String[/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-1.el7_6.x86_64/jre]
```

###  执行多行表达式，赋值给临时变量，返回一个List 

```bash
$ ognl '#value1=@System@getProperty("java.home"), #value2=@System@getProperty("java.runtime.name"), {#value1, #value2}'
@ArrayList[
    @String[/opt/java/8.0.181-zulu/jre],
    @String[OpenJDK Runtime Environment],
]
```
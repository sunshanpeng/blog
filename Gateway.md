## 示例

https示例

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: ext-host-gwy # gateway的名字
spec:
  selector:
    app: my-gateway-controller #gateway envoy pod的标签,可以跨namespace
  servers:
  - port: #对外监听的端口
      number: 443
      name: https
      protocol: HTTPS
    hosts:
    - ext-host.example.com
    - *.example.cn
    - '*' #可以重复
    tls:
      mode: SIMPLE
      serverCertificate: /tmp/tls.crt
      privateKey: /tmp/tls.key
```

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: virtual-svc
spec:
  hosts:
  - ext-host.example.com
  gateways: #默认mesh，表示只能在网格内访问
  - ext-host-gwy #绑定网关
  - istio-gateway.istio-system.svc.cluster.local #跨namespace
  - istio-system/public-gateway #跨namespace
  - mesh #默认
```


## 示例



```yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: my-destination-rule
spec:
  host: my-svc
  trafficPolicy: #负载均衡策略,可以指定：simple、consistentHash、localityLbSetting
    loadBalancer:
      simple: RANDOM #RANDOM、ROUND_ROBIN、LEAST_CONN、PASSTHROUGH
    tls:
      mode: MUTUAL #DISABLE、SIMPLE、MUTUAL、ISTIO_MUTUAL
      clientCertificate: /etc/certs/myclientcert.pem
      privateKey: /etc/certs/client_private_key.pem
      caCertificates: /etc/certs/rootcacerts.pem
  subsets:
  - name: v1
    labels:
      version: v1 #匹配Pod上的label
  - name: v2
    labels:
      version: v2
    trafficPolicy: #子集中指定的策略优先级高于默认
      loadBalancer:
        simple: ROUND_ROBIN
      connectionPool: #连接池配置
        tcp:
          maxConnections: 100 #最大连接数
          connectTimeout: 300ms #连接超时时间
          tcpKeepalive:
            time: 7200s
            interval: 75s
        http:
          http1MaxPendingRequests: 1 #
          maxRequestsPerConnection: 1 #
          http2MaxRequests: 1000 #
          maxRetries: 3 #
      outlierDetection:
        baseEjectionTime: 180.000s
        consecutiveErrors: 1
        interval: 1.000s
        maxEjectionPercent: 100
  - name: v3
    labels:
      version: v3
```


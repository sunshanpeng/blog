## 监控介绍

监控将系统和应用程序生成的指标转换为对应的业务价值，指出哪些组件不起作用或者导致服务质量下降。

监控提供了大量的数据，帮助洞察关键的产品和技术决策。

没有监控，就无法了解系统环境、进行故障诊断、制定容量计划，也无法提供系统的性能、成本和状态等信息。

一个良好的监控系统应该提供以下内容：

- 全局视角，从最高层（业务）依次展开。
- 协助故障诊断。
- 作为基础设施、应用程序开发和业务人员的信息源。
- 内置于应用程序设计、开发和部署的生命周期中。
- 尽可能自动化，并提供自服务。

### 监控机制

**探针和内省**

探针：在应用程序的外部，查询应用程序的外部特征，比如端口是否有响应并返回正确的数据。

内省：主要应用程序内部的内容。将事件、日志和指标发送到监控工具或者由监控工具收集。

**拉取和推送**

**监控数据类型**

指标：指标存储为时间序列数据，用于记录应用程序度量的状态。

日志：日志是从应用程序发出的事件。

### 指标

指标是软件或硬件组件属性的度量。

为了使指标有价值，我们会跟踪器状态，通常记录一段时间内的数据点（也称为观察点），通常包括值、时间戳，有时也涵盖描述观察点的一系列属性。观察的集合称为时间序列。

通常以固定的时间间隔收集数据，称为颗粒度或分辨率。

#### 指标类型

测量型（gauge）：上下增减的数字，特定度量的快照。如CPU、内存、磁盘使用率。

计数型（counter）：随时间增加而不会减少的数字，特定时间重置为零并再次开始递增。如系统运行时间、一个月内的销售数量。

> 计数型指标可以计算变化率。每个观察到的数值都是在一个时刻t，使用t+1处的值减去t处的值以获得两个值之间的变化率。比如登录次数，订单量等。

直方图（histogram）：对观察点进行采样的指标类型，展现数据集的频率分布。

#### 指标摘要

计数：计算特定时间间隔内的观察点数。

求和：将特定时间间隔内所有观察点的值累计相加。

平均值：提供特定时间间隔内所有值的平均值。

中间数：数值的几何中点，正好50%的数值位于它前面，另外50%位于它后面。

百分位数：度量占总数特定百分比的观察点的值。

标准差：指标分布中与平均值的标准差。

变化率：时间序列中数据之间的变化程度。

### Google的四个黄金指标

延迟：服务请求所花费的时间，需要区分成功请求和失败请求。

流量：针对系统，例如每秒HTTP请求数或者数据库的事务。

错误：请求失败的速率，HTTP 500错误或返回错误内容或无效内容等。

> 基于策略原因导致的失败：比如强制要求响应时间超过1m的请求视为错误。

饱和度：应用程序有多“满”。

### 警报和通知

警报会在某些事情发生（如指标达到阀值）时触发。

通知接收警报并告知某人或某事。

建立一个出色的通知系统需要考虑以下基础信息：

- 哪些问题需要通知
- 通知谁
- 怎么样通知
- 多久通知一次
- 何时停止通知以及何时升级到其他人

配置不当的通知可能导致两个后果：

1. 重要的通知没有被正确的通知到对应的人
2. 通知太多而出现警报疲劳，从而忽略

配置通知时需要重点关注以下内容：

1. 使通知清晰、准确、可操作（使用由人而不是计算机编写的通知）。
2. 为通知添加上下文（包含组件的其他相关信息）。
3. 仅发送有意义的通知。

### 可视化

- 清晰的显示数据
- 引发思考
- 避免扭曲数据
- 使数据集一致
- 允许更改颗粒度而不影响理解

## Prometheus介绍

### 名词解释

端点（endpoint）：可以抓取的指标来源，对应单个进程、主机、服务或应用程序。

目标（target）：执行抓取所需的信息，比如如何进行连接，要应用哪些元数据，连接需要哪些身份验证等。

作业（job）：具有相同角色的一组目标。

### 数据模型

<time series name>{<label name>=<label value>,...}

指标名称：描述收集的时间序列数据的一般性质，可以包含ASCII字符、数字、下划线和冒号。

标签：为特定时间序列添加上下文。

- 插桩标签：来自被监控的资源，在抓取之前被添加到时间序列中。
- 目标标签：与架构相关，在抓取期间和之后添加。

采样数据：时间序列的真实值。包含一个float64类型的数值和一个毫秒精度的时间戳。

### 配置文件

```yaml
# my global config
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
       - localhost:9093

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
   - "first_rules.yml"
  # - "second_rules.yml"

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'
    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.
    static_configs:
    - targets: ['localhost:9090']
  # 采集node exporter监控数据
  - job_name: 'node'
    static_configs:
      - targets: ['localhost:9100']
```

global：控制Prometheus行为的全局配置。

- scrape_interval：时间序列的颗粒度，指定应用程序或服务抓取数据的时间间隔。（最好确保所有时间序列具有相同的颗粒度）
- evaluation_interval：评估记录规则和警报规则的频率。

alerting：设置Prometheus的警报。即Alertmanager的地址。

rule_files：指定包含记录规则或警报规则的文件列表。

scrape_configs：指定Prometheus抓取的所有目标。

### 启动参数

--storage.tsdb.path：数据目录。

--storage.tsdb.retention：时间序列的保留期，默认15天。

--config.file：配置文件。

### 标签

标签提供了时间序列的维度，可以定义目标，并为时间序列提供上下文。

标签结合指标名称，构成了时间序列的标识。

数据抓取的生命周期为：

`服务发现->配置->重新标记->抓取->重新标记`

有两处重新标记指标的部分。

#### 拓扑标签（topological label）

通过物理或逻辑组成来切割服务组件，例如job和instance。

#### 模式标签（schematic label）

业务数据，比如url、error_code、user之类的东西。

#### 重新标记

控制、管理并标准化环境中的指标。

- 删除不必要的指标。
- 从指标中删除敏感或不需要的标签。
- 添加、编辑或修改指标的标签值或标签格式。

有两个阶段可以重新标记：

1. 对来自服务发现的目标进行重新标记。在作业内的relabel_configs块中完成。
2. 抓取之后且指标被保存于存储系统之前。在作业内的metric_relabel_configs块中完成。


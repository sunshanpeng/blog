## 安装

### 二进制

```bash
#下载
wget https://github.com/chaosblade-io/chaosblade/releases/download/v0.3.0/chaosblade-0.3.0.linux-amd64.tar.gz
#解压
tar -zxvf chaosblade-0.3.0.linux-amd64.tar.gz
#运行
cd chaosblade-0.3.0/
./blade version
```

## 命令

命令介绍

最主要的命令是创建命令`create`和创建后的销毁命令`destroy`。

创建了以后一定要销毁！！！

创建了以后一定要销毁！！！

创建了以后一定要销毁！！！

`prepare` 和 `revoke` 命令调用混沌实验准备执行器准备或者恢复实验环境。

 混沌实验和混沌实验环境准备记录都可以通过`status` 命令查询。

```bash
[root@master4 ~]# ./blade help
An easy to use and powerful chaos engineering experiment toolkit

Usage:
  blade [command]

Available Commands:
  create      Create a chaos engineering experiment
  destroy     Destroy a chaos experiment
  help        Help about any command
  prepare     Prepare to experiment
  query       Query the parameter values required for chaos experiments
  revoke      Undo chaos engineering experiment preparation
  server      Server mode starts, exposes web services
  status      Query preparation stage or experiment status
  version     Print version info

Flags:
  -d, --debug   Set client to DEBUG mode
  -h, --help    help for blade

Use "blade [command] --help" for more information about a command.
```

创建命令

可以创建CPU、内存、磁盘、网络、进程、脚本、Docker、K8S的故障场景。

```bash
[root@master4 ~]# blade create help
Create a chaos engineering experiment

Usage:
  blade create [command]

Aliases:
  create, c

Examples:
create dubbo delay --time 3000 --offset 100 --service com.example.Service --consumer

Available Commands:
  cplus       c++ experiment
  cpu         Cpu experiment
  disk        Disk experiment
  docker      Execute a docker experiment
  druid       Druid experiment
  dubbo       dubbo experiment
  http        http experiment
  jedis       jedis experiment
  jvm         method
  k8s         Kubernetes experiment
  mem         Mem experiment
  mysql       mysql experiment
  network     Network experiment
  process     Process experiment
  psql        Postgrelsql experiment
  rocketmq    Rocketmq experiment,can make message send or pull delay and exception
  script      Script chaos experiment
  servlet     java servlet experiment

Flags:
  -h, --help   help for create

Global Flags:
  -d, --debug   Set client to DEBUG mode

Use "blade create [command] --help" for more information about a command.
```

## CPU

### CPU满负载

```bash
[root@master4 chaosblade-0.3.0]# ./blade create cpu fullload
{"code":200,"success":true,"result":"4077e008d9a1b63b"}
```

返回参数中的`result`是`Uid`，后续`destroy`命令需要用到。

### 指定CPU负载

```bash
[root@master4 chaosblade-0.3.0]# ./blade create cpu fullload --cpu-percent 80
{"code":200,"success":true,"result":"8b465a05ec5f3468"}
```

80%负载。

另外也可以使用`--cpu-count`指定满负载的CPU个数或者`--cpu-list`指定具体满负载的CPU。

### 指定故障时间

```bash
[root@master4 chaosblade-0.3.0]# ./blade create cpu fullload --cpu-percent 80 --timeout 10
{"code":200,"success":true,"result":"0136822f8f505c61"}
```

`--timeout`可以指定该故障的持续时间，单位是秒，指定持续时间后自动终止该故障。

**后续的其他故障也都可以指定故障时间。**

### 查看CPU负载

- `top`
- `iostat -c 1 1000`

## 内存

### 指定内存负载

```bash
[root@master4 chaosblade-0.3.0]# ./blade c mem load --mem-percent 90
{"code":200,"success":true,"result":"55d737f88301d0a5"}
```

### 查看内存负载

- `top`
- `free -lh`

## 磁盘

### 磁盘容量不足

```bash
[root@master4 chaosblade-0.3.0]# ./blade create disk fill --path /home --size 60000
{"code":200,"success":true,"result":"5e16812a5393e610"}
```

在指定目录`--path`填充指定大小`--size`的数据，单位是M。

### 查看磁盘容量

```bash
[root@master4 chaosblade-0.3.0]# df -h /home
Filesystem               Size  Used Avail Use% Mounted on
/dev/mapper/centos-root   58G  5.4G   53G  10% /

[root@master4 chaosblade-0.3.0]# df -h /home
Filesystem               Size  Used Avail Use% Mounted on
/dev/mapper/centos-root   58G   53G  5.1G  92% /
```

###  磁盘读 IO高负载

```bash
./blade create disk burn --read --path /home
```

### 磁盘写IO高负载

```bash
./blade create disk burn --write --path /home
```

### 查看IO负载

```bash
iostat -x -t 2
```

## 网络

### 网络延时

#### 参数

```
--destination-ip string   目标 IP. 支持通过子网掩码来指定一个网段的IP地址, 例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32。
--exclude-port string     排除掉的端口，可以指定多个，使用逗号分隔或者连接符表示范围，例如 22,8000 或者 8000-8010。 这个参数不能与 --local-port 或者 --remote-port 参数一起使用
--interface string        网卡设备，例如 eth0 (必要参数)
--local-port string       本地端口，一般是本机暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--offset string           延迟时间上下浮动的值, 单位是毫秒
--remote-port string      远程端口，一般是要访问的外部暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--time string             延迟时间，单位是毫秒 (必要参数)
--timeout string          设定运行时长，单位是秒，通用参数
```

#### 指定本地端口延时

所有其他服务器访问本地端口延时。

```bash
[root@master4 chaosblade-0.3.0]# ./blade create network delay --time 3000 --offset 1000 --interface eno16780032 --local-port 9100
{"code":200,"success":true,"result":"88626fd1175317ff"}
```

 在另一台机器验证：` telnet xxx.xxx.xxx.xxx 9100 `

#### 到远程IP端口延时

当前机器访问远程IP端口延时，其他机器不受影响。

```bash
[root@master4 chaosblade-0.3.0]# ./blade create network delay --time 3000 --interface eno16780032 --remote-port 8080 --destination-ip 10.22.19.8
{"code":200,"success":true,"result":"fb75e208d0793104"}
```

演练机器验证：

```bash
[root@master4 chaosblade-0.3.0]# curl -o /dev/null -w %{time_namelookup}::%{time_connect}::%{time_starttransfer}::%{time_total}::%{speed_download}"\n" "http://10.22.19.8:8080/"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 11266    0 11266    0     0   1251      0 --:--:--  0:00:09 --:--:--  2819
0.000::3.005::6.009::9.002::1251.000
```

同时间正常机器：

```bash
[root@master1 ~]# curl -o /dev/null -w %{time_namelookup}::%{time_connect}::%{time_starttransfer}::%{time_total}::%{speed_download}"\n" "http://10.22.19.8:8080/"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 11266    0 11266    0     0  4998k      0 --:--:-- --:--:-- --:--:-- 5500k
0.000::0.001::0.002::0.002::5118582.000
```

#### 让一个容器服务超时

首先找到该服务的宿主机，再找到容器对应的网卡，然后在服务宿主机上设置该网卡超时。

```bash
./blade create network delay --time 3000 --interface cali4c37fec8d88 --timeout 30
```

### 网络丢包

#### 参数

```bash
--destination-ip string   目标 IP. 支持通过子网掩码来指定一个网段的IP地址, 例如 192.168.1.0/24. 则 192.168.1.0~192.168.1.255 都生效。你也可以指定固定的 IP，如 192.168.1.1 或者 192.168.1.1/32。
--exclude-port string     排除掉的端口，可以指定多个，使用逗号分隔或者连接符表示范围，例如 22,8000 或者 8000-8010。 这个参数不能与 --local-port 或者 --remote-port 参数一起使用
--interface string        网卡设备，例如 eth0 (必要参数)
--local-port string       本地端口，一般是本机暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--percent string          丢包百分比，取值在[0, 100]的正整数 (必要参数)
--remote-port string      远程端口，一般是要访问的外部暴露服务的端口。可以指定多个，使用逗号分隔或者连接符表示范围，例如 80,8000-8080
--timeout string          设定运行时长，单位是秒，通用参数
```

#### 指定本地端口丢包

所有其他服务器访问本地端口延时。

```bash
[root@master4 chaosblade-0.3.0]# ./blade create network loss --percent 70 --interface eno16780032 --local-port 9100
{"code":200,"success":true,"result":"88626fd1175317ff"}
```

 在另一台机器验证： `curl  xxx.xxx.xxx.xxx:8080`，不使用 telnet 的原因是 telnet 内部有重试机制，影响实验验证。如果将 percent 的值设置为 100，可以使用 telnet 验证 

#### 到远程IP端口丢包

当前机器访问远程IP端口延时，其他机器不受影响。

```bash
[root@master4 chaosblade-0.3.0]# ./blade create network loss --percent 70 --interface eno16780032 --remote-port 8080 --destination-ip 10.22.19.8
{"code":200,"success":true,"result":"fb75e208d0793104"}
```

演练机器验证：

```bash
[root@master4 chaosblade-0.3.0]# curl -o /dev/null -w %{time_namelookup}::%{time_connect}::%{time_starttransfer}::%{time_total}::%{speed_download}"\n" "http://10.22.19.8:8080/"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 11266    0 11266    0     0   1502      0 --:--:--  0:00:07 --:--:--  3223
0.000::7.082::7.499::7.499::1502.000
```

同时间正常机器：

```bash
[root@master1 ~]# curl -o /dev/null -w %{time_namelookup}::%{time_connect}::%{time_starttransfer}::%{time_total}::%{speed_download}"\n" "http://10.22.19.8:8080/"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 11266    0 11266    0     0  5254k      0 --:--:-- --:--:-- --:--:-- 10.7M
0.000::0.000::0.002::0.002::5380133.000
```

#### 让一个容器服务丢包

首先找到该服务的宿主机，再找到容器对应的网卡，然后在服务宿主机上设置该网卡丢包。

```bash
./blade create network loss --percent 70 --interface cali4c37fec8d88 --timeout 30
```

### 网络隔离

丢包率100%即网络隔离。

### DNS解析

```bash
[root@master4 chaosblade-0.3.0]# ./blade create network dns --domain www.baidu.com --ip 10.0.0.0
{"code":200,"success":true,"result":"5923c8feae34ec52"}
[root@master4 chaosblade-0.3.0]# ping www.baidu.com
PING www.baidu.com (10.0.0.0) 56(84) bytes of data.
^C
--- www.baidu.com ping statistics ---
24 packets transmitted, 0 received, 100% packet loss, time 23533ms
```



## 销毁实验

```bash
[root@master4 chaosblade-0.3.0]# ./blade destroy a6fa3362742e8c6f
{"code":200,"success":true,"result":"command: network loss --help false --interface eno16780032 --debug false --local-port 9100 --percent 70, destroy time: 2019-10-22T17:35:36.557547578+08:00"}
```

每个实验要么在创建的时候指定销毁时间`--timeout`，要么手动销毁，不然会一直执行。

## 查看实验状态

创建只会如果忘记了uid，可以通过status命令查询。

```bash
[root@master4 chaosblade-0.3.0]# ./blade status --type create
{
        "code": 200,
        "success": true,
        "result": [
                {
                        "Uid": "4cdc5e72e623d449",
                        "Command": "network",
                        "SubCommand": "delay",
                        "Flag": "--debug false --interface cali4c37fec8d88 --time 3000 --timeout 30 --help false",
                        "Status": "Destroyed",
                        "Error": "",
                        "CreateTime": "2019-10-22T16:35:53.572066698+08:00",
                        "UpdateTime": "2019-10-22T16:36:23.898669337+08:00"
                },
                {
                        "Uid": "01e7e9b19770834c",
                        "Command": "network",
                        "SubCommand": "delay",
                        "Flag": "--timeout 60 --debug false --interface cali4c37fec8d88 --time 3000 --help false",
                        "Status": "Destroyed",
                        "Error": "",
                        "CreateTime": "2019-10-22T16:42:30.821076685+08:00",
                        "UpdateTime": "2019-10-22T16:43:30.937855907+08:00"
                }
        ]
}
```


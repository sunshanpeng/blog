---
title: Kubernetes介绍
date: 2019-02-24 22:52:37
categories:  
 - 容器
tags: 
 - k8s

---
有了Docker之后，单个应用进程的使用管理变的更简单了，但是如果想要管理一个集群的不同应用，处理应用之间的关系，

比如：一个Web应用与数据库之间的访问关系，负载均衡和后端服务之间的代理关系，门户应用与授权组件的调用关系，

再比如说一个服务单位的不同功能之间的关系，如一个Web应用与日志搜集组件之间的文件交换关系，

在传统虚拟机环境对这种关系的处理方法都比较“粗颗粒”，一股脑的部署在同一台虚拟机中，需要手动维护很多跟应用协作的守护进程，用来处理它的日志搜集、灾难恢复、数据备份等辅助工作。

使用容器和编排技术以后，那些原来挤在同一个虚拟机里的各个应用、组件、守护进程，都可以被分别做成镜像，然后运行在一个个专属的容器中。

它们之间互不干涉，拥有各自的资源配额，可以被调度在整个集群里的任何一台机器上。

如果只是用来封装微服务、拉取用户镜像、调度单容器，用Docker Swarm就足够了，而且方便有效，但是如果需要路由网关、水平扩展、弹性伸缩、监控、备份、灾难恢复等一系列运维能力，就需要使用kubernetes来解决了。

## Kubernetes介绍
![kubernetes](http://image.sunshanpeng.com/timg.jpg)

Kubernetes是Google开源的容器编排调度引擎，它的目标不仅仅是一个编排系统，而是提供一个规范，**可以用来描述集群的架构，定义服务的最终状态**，Kubernetes可以将系统自动得到和维持在这个状态。

更直白的说，Kubernetes用户可以通过编写一个yaml或者json格式的配置文件，也可以通过工具/代码生成或直接请求Kubernetes API创建应用，该配置文件中包含了用户想要应用程序保持的状态，不论整个Kubernetes集群中的个别主机发生什么问题，都不会影响应用程序的状态，你还可以通过改变该配置文件或请求Kubernetes API来改变应用程序的状态。

**配置文件里面怎么定义的，整个系统就是怎么运行的。**

## kubernetes 的马斯洛需求

![image](http://image.sunshanpeng.com/cae7afc329d743429328887cb831d4c5.jpg)

## Kubernetes架构图

![img](http://image.sunshanpeng.com/Yrdoo02.jpg)

Kubernets属于主从的分布式集群架构，包含Master和Node：

1. Master作为控制节点，调度管理整个系统，包含以下组件：

   - API Server作为kubernetes系统的入口，封装了核心对象的增删改查操作，以RESTFul接口方式提供给外部客户和内部组件调用。它维护的REST对象将持久化到etcd(一个分布式强一致性的key/value存储)。
   - Scheduler：负责集群的资源调度，为新建的pod分配机器。这部分工作分出来变成一个组件，意味着可以很方便地替换成其他的调度器。
   - Controller Manager：负责执行各种控制器，目前有两类：
     （1）Endpoint Controller：定期关联service和pod(关联信息由endpoint对象维护)，保证service到pod的映射总是最新的。
     （2）Replication Controller：定期关联replicationController和pod，保证replicationController定义的复制数量与实际运行pod的数量总是一致的。

2. Node是运行节点，运行业务容器，包含以下组件：

   - Kubelet：负责管控docker容器，如启动/停止、监控运行状态等。它会定期从etcd获取分配到本机的pod，并根据pod信息启动或停止相应的容器。同时，它也会接收apiserver的HTTP请求，汇报pod的运行状态。
   - Kube Proxy：负责为pod提供代理。它会定期从etcd获取所有的service，并根据service信息创建代理。当某个客户pod要访问其他pod时，访问请求会经过本机proxy做转发。

   > Kubernets使用Etcd作为存储和通信中间件，实现Master和Node的一致性，这是目前比较常见的做法，典型的SOA架构，解耦Master和Node。

## 基本概念

![img](http://image.sunshanpeng.com/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20181017210240.png)

- ### Pod

  Pod是Kubernetes的基本操作单元，把相关的一个或多个容器构成一个Pod，通常Pod里的容器运行相同的应用。Pod包含的容器运行在同一个Node(Host)上，看作一个统一管理单元，共享相同的volumes和network namespace/IP和Port空间。

- ### Replication Controller

  Replication Controller确保任何时候Kubernetes集群中有指定数量的pod副本(replicas)在运行， 如果少于指定数量的pod副本(replicas)，Replication Controller会启动新的Container，反之会杀死多余的以保证数量不变。

  Replication Controller使用预先定义的pod模板创建pods，一旦创建成功，pod 模板和创建的pods没有任何关联，可以修改pod 模板而不会对已创建pods有任何影响，也可以直接更新通过Replication Controller创建的pods。

  对于利用pod 模板创建的pods，Replication Controller根据label selector来关联，通过修改pods的label可以删除对应的pods。

- ### Service

  Service也是Kubernetes的基本操作单元，是真实应用服务的抽象，每一个服务后面都有很多对应的容器来支持，通过Proxy的port和服务selector决定服务请求传递给后端提供服务的容器，对外表现为一个单一访问接口，外部不需要了解后端如何运行，这给扩展或维护后端带来很大的好处。

- ### Label

  Label是用于区分Pod、Service、Replication Controller的key/value键值对，Pod、Service、 Replication Controller可以有多个label，但是每个label的key只能对应一个value。Labels是Service和Replication Controller运行的基础，为了将访问Service的请求转发给后端提供服务的多个容器，正是通过标识容器的labels来选择正确的容器。同样，Replication Controller也使用labels来管理通过pod 模板创建的一组容器，这样Replication Controller可以更加容易，方便地管理多个容器，无论有多少容器。

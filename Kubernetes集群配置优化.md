---
title: Kubernetes集群配置优化
date: 2019-03-24 22:26:37
categories:  
 - 容器
tags: 
 - docker
 - k8s
---

##  一、Docker配置

```json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "log-driver": "json-file",
    "log-opts": {
        "max-size": "100m",
        "max-file": "10"
    },
    "bip": "169.254.123.1/24",
    "oom-score-adjust": -1000,
    "registry-mirrors": ["https://registry.docker-cn.com", "https://docker.mirrors.ustc.edu.cn"],
    "storage-driver": "overlay2",
    "storage-opts":["overlay2.override_kernel_check=true"],
    "data-root": "/var/lib/docker",
    "live-restore": true
}
```

### data-root

设置存储持久数据和资源配置的目录，默认值`/var/lib/docker`，**17.05.0**以下版本参数为：`graph`。

> 如果只有一个系统盘可以不用指定该参数，如果有数据盘并且数据盘不是`/var`，则需要指定数据目录。

### registry-mirrors

镜像仓库地址，用于镜像加速。

> 直接连接国外dockerHub拉取镜像速度太慢，一般都会配置国内的镜像地址。

### bip

设置docker0网桥地址，默认是`172.17.0.1/16`。

> 默认网段有可能和现有服务器的网段冲突，建议设置成`169.254.123.1/24`

### live-restore

当docker daemon停止时，让容器继续运行，默认`false`关闭，设为`true`打开。

> 默认情况下重启或者关闭docker daemon时，容器都会停止，通过设置live-restore让容器继续运行，在修改docker配置或者升级docker的时候很有用。（如果daemon重启之后使用了不同的bip或不同的data-root，则live restore无法正常工作）

### log-opts

docker日志设置，默认为空。

> 不设置最大值的话日志文件可能会撑爆硬盘。

### exec-opts

设置docker cgroup驱动。

> 需要注意docker的Cgroups和kubelet的Cgroups配置是否一致。

## 二、Kubelet配置

```
--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf 
--kubeconfig=/etc/kubernetes/kubelet.conf 
--pod-manifest-path=/etc/kubernetes/manifests 
--allow-privileged=true --network-plugin=cni 
--cni-conf-dir=/etc/cni/net.d 
--cni-bin-dir=/opt/cni/bin 
--cluster-dns=192.168.176.10 
--pod-infra-container-image=registry-vpc.cn-shanghai.aliyuncs.com/acs/pause-amd64:3.1 
--enable-controller-attach-detach=false 
--enable-load-reader 
--cluster-domain=cluster.local 
--cloud-provider=external 
--hostname-override=$(NODE_NAME)
--provider-id=cn-shanghai.i-uf657oy682jirk1oad62 
--authorization-mode=Webhook 
--client-ca-file=/etc/kubernetes/pki/ca.crt 
--system-reserved=memory=300Mi 
--kube-reserved=memory=400Mi 
--eviction-hard=imagefs.available<15%,memory.available<300Mi,nodefs.available<10%,nodefs.inodesFree<5% 
--cgroup-driver=systemd 
--anonymous-auth=false 
--rotate-certificates=true 
--cert-dir=/var/lib/kubelet/pki
--root-dir=/var/lib/kubelet
```

### pod-infra-container-image

基础镜像，默认使用的`k8s.gcr.io/pause-amd64:3.1`需要翻墙，可以使用国内镜像或者自己上传的镜像。

### root-dir

设置存储持久数据和资源配置的目录，默认值`/var/lib/kubelet`。

> 用法同Docker的data-root参数。

### cgroup-driver

设置kubelet Cgroups驱动。

> 需要注意docker的Cgroups和kubelet的Cgroups配置是否一致。

### system-reserved

给系统保留的资源大小，可以设置CPU、内存、硬盘。

### kube-reserved

给kubernetes保留的资源大小，可以设置CPU、内存、硬盘。

### eviction-hard

驱逐条件，当节点资源小于设置的驱逐条件时，kubelet开始驱逐Pod。

## 三、Kube-proxy

```
        - --proxy-mode=ipvs
        - --kubeconfig=/var/lib/kube-proxy/kubeconfig.conf
        - --cluster-cidr=172.17.0.0/18
        - --hostname-override=$(NODE_NAME)
```

### proxy-mode

ipvs可以大大提高性能。

## 参考资料：

- <https://docs.docker.com/engine/reference/commandline/dockerd/>
- <https://kubernetes.io/docs/tasks/administer-cluster/reserve-compute-resources/>
- <https://kubernetes.io/docs/tasks/administer-cluster/out-of-resource/>
- <https://docs.docker.com/engine/deprecated/>
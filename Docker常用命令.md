---
title: Docker常用命令
date: 2019-03-24 22:26:37
categories:  
 - 容器
tags: 
 - docker

---

## 拉取镜像

```bash
docker pull mysql:5.6
```

格式：`docker pull [OPTIONS] NAME[:TAG|@DIGEST]`，tag不写的话默认latest。

## 登录镜像仓库

```bash
docker login --username sunshanpeng --password 123456 harbor.sunshanpeng.com
```

格式：`docker login [OPTIONS] [SERVER]`

- SERVER为空的情况下默认登录`hub.docker.com`

- password建议不要显示在控制台上

## 列出所有镜像

```bash
docker images
```

格式：`docker images [OPTIONS] [REPOSITORY[:TAG]]`，可选参数显示全部镜像或者过滤镜像。

## 列出所有容器

```bash
docker ps -a
```

格式：`docker ps [OPTIONS]`，去掉`-a`显示运行中的容器。

## 给镜像打tag

```bash
docker tag mysql:5.6 harbor.sunshanpeng.com/database/mysql:5.6
```

格式：`docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]`

当我们从公共仓库拉取的镜像，想推送到我们自己搭建的私有仓库，需要先打一个tag；

当我们拉取不到一些需要翻墙才能拉取的镜像时，也可以从国内镜像仓库拉取镜像然后打个tag给成目标镜像。

## 推送镜像

```bash
docker push harbor.sunshanpeng.com/database/mysql:5.6
```

格式：`docker push [OPTIONS] NAME[:TAG]`

推送镜像必须先登录要推送的目标仓库，而且需要对目标仓库有推送的权限。

类似`docker push mysql:5.6`是不行的。

## 制作镜像

```bash
docker build -t simpleApp:v1 .
```

格式：`docker build [OPTIONS] PATH | URL | -`。

根据`Dockerfile`制作镜像。

## 删除镜像

```bash
docker rmi mysql:5.6
```

格式：`docker rmi [OPTIONS] IMAGE [IMAGE...]`，不能删除已经创建了容器的镜像，必须先删除对应容器。

## 运行容器

```bash
docker run nginx
```

格式：`docker run [OPTIONS] IMAGE [COMMAND] [ARG...]`

OPTIONS参数有很多，比较常用的如下：

- `--name`指定容器名字

- `-p`指定容器映射宿主机的端口号

- `-e`指定环境变量

- `-v`指定挂载卷

- `-it`指定容器前台运行

- `-d`指定容器后台运行

  其他还有很多指定IP、Hostname、CPU、内存、网络、内核参数、重启策略等等参数，可以通过`docker run --help`查询。

  ### mysql容器启动命令

  ```bash
  docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -v=/docker/mysql/data:/var/lib/mysql -v=/docker/mysql/my.cnf:/etc/mysql/my.cnf -v=/usr/share/zoneinfo/Asia/Shanghai:/etc/localtime -d mysql:5.6
  ```

  ### redis容器启动命令

  ```bash
  docker run --name redis -p 6379:6379 -v=/docker/redis/data:/data -d redis:3.2
  ```

## 重命名容器

```bash
docker rename mysql mysql1
```

格式：`docker rename CONTAINER NEW_NAME`。

## 停止容器

```bash
docker stop mysql
```

格式：`docker stop [OPTIONS] CONTAINER [CONTAINER...]`，停止多个容器用空格分隔容器名字或者容器id。

## 强制停止容器

```bash
docker kill mysql
```

格式：`docker kill [OPTIONS] CONTAINER [CONTAINER...]`。

## 启动/重启容器

```bash
docker start/restart mysql
```

格式：`docker start/restart [OPTIONS] CONTAINER [CONTAINER...]`。

## 删除容器

```bash
docker rm mysql
```

格式：`docker rm [OPTIONS] CONTAINER [CONTAINER...]`，不带参数时只能删除已停止的容器，带上参数`-f`可以强制删除运行中的容器。

## 更新容器

```bash
docker update --restart always mysql
```

格式：`docker update [OPTIONS] CONTAINER [CONTAINER...]`。

更新容器的重启策略、CPU和内存大小。

## 进入容器

```bash
docker exec -it mysql /bin/bash
```

格式：`docker exec [OPTIONS] CONTAINER COMMAND [ARG...]`。

- CONTAINER 可以是容器Id或者容器名字；

- `/bin/bash`是进入容器后执行的命令，有些容器可能不存在`/bin/bash`。

## 导出镜像文件

```bash
docker save -o rook-ceph-093.tar rook/ceph:v0.9.3
```

格式：`docker save [OPTIONS] IMAGE [IMAGE...]`。

如果同时导出多个镜像到一个文件里，后面的镜像列表用空格来分隔。

## 导入镜像文件

```bash
docker load -i /root/rook-ceph-093.tar
```

格式：`docker load [OPTIONS]`

当不方便使用自建的镜像仓库来传输镜像时，可以用镜像的导出导入功能来移动镜像。

## 查看容器资源消耗

```bash
docker stats
CONTAINER ID        NAME                               CPU %               MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O           PIDS
63ebc0e88e5a        happy_volhard                      0.02%               35.3MiB / 15.46GiB    0.22%               11.5MB / 19MB       13.3MB / 45MB       22
7ed66f584a25        redis                              0.08%               7.82MiB / 15.46GiB    0.05%               304kB / 274kB       5.59MB / 0B         3
500eed21d914        dockercompose_mqbroker_1           2.98%               752.3MiB / 15.46GiB   4.75%               0B / 0B             3.07GB / 19GB       204
ddd919a5bf52        dockercompose_mqnamesrv_1          0.21%               329.2MiB / 15.46GiB   2.08%               0B / 0B             60.3MB / 16.4kB     41
```

## 查看Docker占用磁盘

```bash
docker system df

TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              29                  28                  8.575GB             1.873GB (21%)
Containers          29                  17                  7.545GB             410.8MB (5%)
Local Volumes       303                 23                  6.339GB             4.138GB (65%)
Build Cache         0                   0                   0B                  0B
```

可以查看镜像文件大小、容器大小、本地存储使用的磁盘大小。

## 清理Docker镜像和容器

```bash
docker system prune -a
```

此命令会清除所有未运行的容器和清理所有未使用的镜像。
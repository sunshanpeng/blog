---
title: Kubernetes高可用部署
date: 2019-08-24 12:29:30
categories:  
 - 容器
tags: 
 - k8s

---
## 基本架构

![kubernetes](http://image.sunshanpeng.com/Yrdoo02.jpg)

Kubernetes分为Master Node和Worker Node，其中Master Node作为控制节点，调度管理整个系统；Worker Node作为工作节点，运行业务容器。

### Master Node

 - #### api-server
    作为kubernetes系统的入口，对外暴露了Kubernetes API，封装了核心对象的增删改查操作，以RESTFul接口方式提供给外部客户和内部组件调用。它维护的REST对象将持久化到etcd(一个分布式强一致性的key/value存储)。

 - #### scheduler
    负责集群的资源调度，为新建的pod分配机器。这部分工作分出来变成一个组件，意味着可以很方便地替换成其他的调度器。

 - #### controller-manager

    运行控制器，它们是处理集群中常规任务的后台线程。逻辑上，每个控制器是一个单独的进程，但为了降低复杂性，它们都被编译成独立的可执行文件，并在单个进程中运行。
    这些控制器包括:
    
    - 节点控制器: 当节点移除时，负责注意和响应。
    - 副本控制器: 负责维护系统中每个副本控制器对象正确数量的 Pod。
    - 端点控制器: 填充 端点(Endpoints) 对象(即连接 Services & Pods)。
    - 服务帐户和令牌控制器: 为新的命名空间创建默认帐户和 API 访问令牌

### Worker Node

- #### kubelet
  kubelet是主要的节点代理,它监测已分配给其节点的 Pod(通过 apiserver 或通过本地配置文件)，提供如下功能:

    - 挂载 Pod 所需要的数据卷(Volume)。
    - 下载 Pod 的 secrets。
    - 通过 Docker 运行(或通过其他容器运行时)运行 Pod 的容器。
    - 周期性的对容器生命周期进行探测。
    - 如果需要，通过创建 镜像 Pod（Mirror Pod） 将 Pod 的状态报告回系统的其余部分。
  将节点的状态报告回系统的其余部分。

- #### kube-proxy
通过维护主机上的网络规则并执行连接转发，实现了Kubernetes服务抽象。

### 存储

Kubernetes 所有集群数据都存储在etcd里。

etcd通常部署在Master Node上，也可以单独部署。

官网地址：https://github.com/etcd-io/etcd

### 网络

CNI（Container Network Interface）是CNCF旗下的一个项目，由一组用于配置Linux容器的网络接口的规范和库组成，同时还包含了一些插件。CNI仅关心容器创建时的网络分配，和当容器被删除时释放网络资源。

每个pod都会有一个集群内唯一的IP，不同Node上的Pod可以互相访问。

常用的网络插件有[Calico](https://github.com/projectcalico/calico)和[Flannel](https://github.com/coreos/flannel)。

### 容器

CRI（Container Runtime Interface）是容器运行时接口，kubelet可以运行实现了CRI的各种容器运行时而不仅仅是Docker。

## 高可用架构

要实现高可用，就要实现所有Node没有单点故障，任何一台服务器宕机均不影响Kubernetes的正常工作。Kubernetes的高可用主要是Master Node和存储的高可用，包括以下几个组件：

- etcd属于CP架构，保证一致性和分区容错性。因为其内部使用 Raft 协议进行选举，所以需要部署基数个实例，比如3、5、7个。可以和Master Node一起部署也可以外置部署。
- controller-manager和scheduler的高可用相对容易，是基于etcd的锁进行leader election。相当于基于etcd的分布式锁，谁拿到谁干活，同时只会有一个实例在工作，部署2个及以上的实例数量。
- api-server是无状态的，需要部署2个及以上的实例数量，然后使用HAProxy和Keepalived作为负载均衡实现高可用。除了HAProxy也可以使用Nginx或其他负载均衡策略。

### 方案一

![Kubernetes HA1](http://image.sunshanpeng.com/201908251648_290.png)

使用三台4核8G的服务器当Master Node，上面部署api-server、scheduler、controller-manager、kubelet、kube-proxy、docker、calico、etcd、HAProxy、keepalived。

Worker Node的配置根据实际情况，最好CPU和内存的大小、比例都一样。上面部署kubelet、kube-proxy、docker、calico。

该方案集群内外连接api-server都是高可用，使用方便。但是公有云不能用虚拟IP所以不能用该方案。另外所有的网络请求都会集中到一台服务器（虚拟IP所在的服务器）进行转发，负载不均衡，HAProxy的上限即api-server的上限。

### 方案二

![Kubernetes HA2](http://image.sunshanpeng.com/201908251741_884.png)

使用三台4核8G的服务器当Master Node，上面部署api-server、scheduler、controller-manager、kubelet、kube-proxy、docker、calico、etcd。

Worker Node的配置根据实际情况，最好CPU和内存的大小、比例都一样。上面部署kubelet、kube-proxy、docker、calico、HAProxy。

该方案兼容自有机房和公有云，Master Node负载比较均衡。但是集群外访问api-server没有做到高可用，需要额外解决。如果Worker Node上的HAProxy宕机则该节点不能正常工作。另外如果添加和删除Master Node需要更新所有Worker Node的负载均衡配置（可选项，不修改也可以）。

## 部署方式

### 二进制部署

所有组件均使用二进制文件部署，手动部署可以参考：<https://github.com/opsnull/follow-me-install-kubernetes-cluster>；ansible脚本部署可以用：<https://github.com/easzlab/kubeasz>。

### 容器化部署

kubernetes的所有组件中，除了docker和kubelet是运行和管理容器的，其他组件都可以使用容器化的方式部署，最流行的方式就是使用kubeadm。

官网地址：https://github.com/kubernetes/kubeadm。

## 参考

https://kubernetes.io/zh/docs/concepts/overview/components/

https://www.kubernetes.org.cn/2931.html

https://jimmysong.io/kubernetes-handbook/concepts/cni.html

https://jimmysong.io/kubernetes-handbook/concepts/cri.html